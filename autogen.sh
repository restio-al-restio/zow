# autogen.sh

# Copyright (C) 2017-2021 Thien-Thi Nguyen
#
# This file is part of ZOW.
#
# ZOW is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 3, or (at your option) any later
# version.
#
# ZOW is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU General Public License
# along with ZOW.  If not, see <https://www.gnu.org/licenses/>.

autoconf --verbose --force --warnings all

# autogen.sh ends here
