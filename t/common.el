;;; common.el                                      -*- lexical-binding: t -*-

;; Copyright (C) 2016-2021 Thien-Thi Nguyen
;;
;; This file is part of ZOW.
;;
;; ZOW is free software; you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free
;; Software Foundation; either version 3, or (at your option) any later
;; version.
;;
;; ZOW is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
;; for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with ZOW.  If not, see <https://www.gnu.org/licenses/>.

;;; Code:

(defun --ztc-barf-if-not-loading (what)
  (unless load-in-progress
    (error "Cannot use %s unless loading" what)))

(--ztc-barf-if-not-loading "common.el")

(setq INIT-current-buffer (current-buffer)
      INIT-default-directory default-directory
      TEST-stem nil                     ; set by ‘hello!’
      TEST-d nil                        ; likewise
      rdir nil                          ; likewise
      wdir nil                          ; likewise
      print-escape-newlines t
      message-log-max t
      visible-bell t
      debug-on-error (equal "1" (getenv "DEBUG"))
      debug-on-quit debug-on-error
      ;; the rest of these are for speed
      vc-handled-backends nil)

(global-font-lock-mode -1)              ; for speed

(require 'compile)
(require 'cl-lib)
(require 'zow)

(defvar-local --ztc-exit-status nil)

(setq compilation-exit-message-function
      (lambda (proc-status exit-status msg)
        (setq --ztc-exit-status exit-status)
        (cons msg exit-status)))

(defun get-exit-status (buf)
  (buffer-local-value '--ztc-exit-status buf))

(defun handle-msg (ctxt body &rest args)
  (if body
      (zow-yeah-echo ctxt body args)
    (cons t (format "no body, just args: %S"
                    args))))

(defconst zow-test-handlers
  '((msg . handle-msg)
    (ok . zow-yeah-echo)))

(defun die (fmt &rest args)
  (apply #'message fmt args)
  (kill-emacs 1))

(defun getenv-dammit (name)
  (or (getenv name)
      (die "Env var ‘%s’ not defined" name)))

(defun getdir (name)
  (expand-file-name (getenv-dammit name)))

(defvar srcdir (getdir "srcdir"))
(defvar topdir (getdir "topdir"))

(defvar *PROBLEMS* 0)                   ; optimistic

(add-hook 'debugger-mode-hook
          (lambda ()
            (cl-incf *PROBLEMS*)))

(defun bye! (&optional no-ask)
  (interactive "P")
  (let ((ev (if (or noninteractive
                    no-ask)
                *PROBLEMS*
              (read-number "Exit value (negative to cancel): "
                           *PROBLEMS*))))
    (if (> 0 ev)
        (message "(‘%s’ cancelled)"
                 (substitute-command-keys
                  "\\[bye!]"))
      (when load-in-progress
        (here "bye!"))
      (kill-emacs ev))))

(defun file-contents (filename)
  (with-temp-buffer
    (insert-file-contents-literally filename)
    (buffer-string)))

(defun compare-buffer-against-file (buffer filename)
  (compare-strings (with-current-buffer buffer
                     (buffer-string))
                   nil nil
                   (file-contents filename)
                   nil nil))

(defun interesting-files (&rest dirs)
  (cl-loop                              ; FIXME: grody to the max
   with ls = (cl-intersection
              '("zow-feed" "zow.sock")
              (directory-files default-directory)
              :test #'string=)
   for dir in dirs
   do (when (and (file-exists-p dir)
                 (file-directory-p dir))
        (cl-loop
         for file in (directory-files dir t "^[^.]")
         when (or (file-symlink-p file)
                  (file-executable-p file))
         collect (file-relative-name file) into plus-x
         finally do (setq ls (cl-union ls plus-x :test #'string=))))
   finally return (sort ls #'string<)))

(defun current-lno ()
  (1+ (count-lines (point-min) (line-beginning-position))))

(defun previously (string &optional n)
  (when (save-excursion
          (search-backward string
                           (when n
                             (line-beginning-position
                              (1+ (- n))))
                           t))
    t))

(defmacro no-worries (&rest body)
  `(progn ,@body t))

(defmacro in-dir (dir &rest body)
  (declare (indent 1))
  `(let ((default-directory (file-name-as-directory ,dir)))
     ,@body))

(defun here (&optional fmt &rest args)
  (--ztc-barf-if-not-loading "func ‘here’")
  (message "%s:%d: %s"
           TEST-stem
           (with-current-buffer " *load*"
             (save-excursion
               (forward-sexp -1)
               (current-lno)))
           (cond ((not fmt) 'here)
                 ((not args) fmt)
                 (t (apply #'format fmt args)))))

(defun hello! (&rest opts)
  (--ztc-barf-if-not-loading "func ‘hello!’")
  (cl-macrolet
      ((must (form fmt &rest args)
             `(condition-case nil
                  ,form
                (error (die ,fmt ,@args)))))
    (setq TEST-stem (file-relative-name load-file-name
                                        INIT-default-directory)
          TEST-d (concat TEST-stem ".d")
          rdir (expand-file-name (concat TEST-d "/x.git"))
          temporary-file-directory (expand-file-name
                                    TEST-d INIT-default-directory))
    (here (must (with-current-buffer " *load*"
                  (save-excursion
                    (goto-char (point-min))
                    (when (re-search-forward "\\( --- .+\\)$" nil t)
                      (format "hello!%s\n --- %s\n --- %s"
                              (match-string 0)
                              (delete ?\n (emacs-version))
                              (zow-version)))))
                "Could not find description on first line of %s"
                TEST-stem))
    (must (progn (when (file-directory-p TEST-d)
                   (delete-directory TEST-d t))
                 (make-directory TEST-d))
          "Could not create directory %s"
          TEST-d)
    (unless (cl-flet
                ((faker (cmd arg)
                        (equal 0 (call-process
                                  (expand-file-name ".faker" srcdir)
                                  nil nil nil
                                  cmd arg))))
              (and (faker "unpack-tarball-in-dir" TEST-d)
                   (file-directory-p rdir)
                   (or (not (memq 'need-wdir opts))
                       (and (faker "create-working-directory-from" rdir)
                            (file-directory-p
                             (setq wdir (expand-file-name
                                         (concat TEST-d "/x"))))))))
      (error "Could not create fake repo%s in %s/"
             (if wdir "s" "")
             TEST-d))
    (when debug-on-error
      (message "                 srcdir => %S" srcdir)
      (message "                 topdir => %S" topdir)
      (message "    INIT-current-buffer => %S" INIT-current-buffer)
      (message "       (current-buffer) => %S" (current-buffer))
      (message " INIT-default-directory => %S" INIT-default-directory)
      (message "      default-directory => %S" default-directory)
      (message "              TEST-stem => %S" TEST-stem)
      (message "                 TEST-d => %S" TEST-d)
      (message "                   rdir => %S" rdir)
      (message "                   wdir => %S" wdir))))

(defmacro report (expression)
  `(let ((rv ,expression))
     (here "%s => %S"
           ,(format "%S" expression)
           rv)
     rv))

(defun okay (fmt &rest args)
  (apply #'here (concat "KAY: " fmt) args))

(defun prob (fmt &rest args)
  (apply #'here (concat "ROB: " fmt) args)
  (cl-incf *PROBLEMS*))

(defun smile-func (object form)
  (let ((form-string (format "%S" form)))
    (condition-case ugh
        (let ((got (eval form)))
          (if (equal object got)
              (okay "%s => %S"
                    form-string
                    object)
            (prob "%s => %S (expected ‘%S’)"
                  form-string
                  got object)))
      (error
       (prob "%s signalled ‘%s’ %S"
             form-string
             (car ugh)
             (cdr ugh))))))

(defmacro smile (object form)
  `(smile-func ,object ',form))

(defun frown-func (signame form)
  (let* ((x-signalled (format "%S signalled" form))
         (expected-but (format "expected signal ‘%S’, but instead %s"
                               signame x-signalled)))
    (condition-case ugh
        (let ((got (eval form)))
          (prob "%s nothing and => ‘%S’"
                expected-but got))
      (error
       (if (eq signame (car ugh))
           (okay "%s ‘%S’ %S"
                 x-signalled
                 (car ugh)
                 (cdr ugh))
         (prob "%s ‘%S’ %S"
               expected-but
               (car ugh)
               (cdr ugh)))))))

(defmacro frown (signame form)
  `(frown-func ,signame ',form))

(defun bg (fmt &rest args)
  (let* ((fmt (concat "set -x ; " fmt))
         (cmd (if args
                  (apply #'format fmt args)
                fmt))
         (buf (compile cmd)))
    (while compilation-in-progress
      (report (accept-process-output nil 0.42))
      (redisplay))
    (when noninteractive
      (message "%s" (with-current-buffer buf
                      (let ((p (progn
                                 (goto-char (point-min))
                                 (line-beginning-position 3)))
                            (q (progn
                                 (goto-char (point-max))
                                 (line-end-position -1))))
                        ;;                ^_^
                        (buffer-substring p q)))))
    buf))

(defun bg-ls (&rest ls)
  (bg (mapconcat #'identity ls " ; ")))

(global-set-key "\C-x\C-c" 'bye!)

(display-buffer "*Messages*")

(when noninteractive
  (message "-*- mode: grep; default-directory: %S -*-"
           (abbreviate-file-name default-directory)))

;;; common.el ends here
