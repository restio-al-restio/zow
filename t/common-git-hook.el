;;; common-git-hook.el                             -*- lexical-binding: t -*-

;; Copyright (C) 2016-2021 Thien-Thi Nguyen
;;
;; This file is part of ZOW.
;;
;; ZOW is free software; you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free
;; Software Foundation; either version 3, or (at your option) any later
;; version.
;;
;; ZOW is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
;; for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with ZOW.  If not, see <https://www.gnu.org/licenses/>.

(require 'git-hook)

(defun wgdir ()
  (expand-file-name ".git" wdir))

(defun sort-symbols (key symbols)
  (mapcar #'intern
          (sort (mapcar #'symbol-name
                        (if key
                            (mapcar key symbols)
                          symbols))
                #'string<)))

(defconst R-NAMES   (sort-symbols #'car git-hook-handlers))
(defconst B-NAMES   (sort-symbols #'car git-hook-local-handlers))
(defconst ALL-NAMES (sort-symbols nil (append R-NAMES
                                              B-NAMES)))

(defun all-interesting-files ()
  (interesting-files "hooks/"))

(defun interesting-smile (names)
  (smile (append (mapcar (lambda (symbol)
                           (format "hooks/%s" symbol))
                         names)
                 '("zow-feed" "zow.sock"))
         (all-interesting-files)))

(defun wd-do (&rest commands)
  (get-exit-status
   (in-dir wdir
     (apply #'bg-ls "set -e" commands))))

;;; common-git-hook.el ends here
