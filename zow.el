;;; zow.el --- server-side Git hooks processing     -*- lexical-binding: t -*-

;; Copyright (C) 2016-2021 Thien-Thi Nguyen
;;
;; This file is part of ZOW.
;;
;; ZOW is free software; you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free
;; Software Foundation; either version 3, or (at your option) any later
;; version.
;;
;; ZOW is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
;; for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with ZOW.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; HEADER has the form (NAME [ARG...]).  NAME is a symbol.
;; Each ARG is the ‘read’able representation of a Lisp object,
;; such as: 'SYMBOL, :KEYWORD, "STRING", NUMBER.
;;
;; The "stage" of a transaction is a form (NUMBER [DATA...]).
;;   0 (maybe) -- not enough input to check magic and protocol version
;;   1 (partial-header) -- no newline yet
;;   2 (fill) -- first line read/decoded; DATA is (NUMLINES HEADER)
;;   3 (input-complete) -- body read; DATA is (HEADER [LINE...])
;;   4 ???
;;
;; Each HANDLER is called, with the server log buffer
;; made current, like so:
;;
;;   (HANDLER CONTEXT BODY [ARG...])
;;
;; For CONTEXT, see ‘zow-ctxt-conn’, ‘zow-ctxt-name’.
;;
;; BODY is a (possibly empty) list of strings
;; representing the lines of the request body.
;;
;; HANDLER should return one of the forms:
;;
;;   RESULT
;;   (RESULT . EXPLANATION)
;;
;; A nil RESULT means failure, and t success.
;; EXPLANATION is a string or a list of strings to
;; be sent back to the client, one per line.
;;
;; A "handler set" (aka "hset") has the form (DIR . ALIST).  ALIST elements
;; have the form (NAME . HANDLER).  When DIR is nil, use NAME directly and
;; avoid modifying the filesystem.  In this case, NAME is considered an
;; "internal" handler.  Otherwise, DIR should be a relative directory name
;; (ending w/ slash), in which case the callable name is the symbol /DIR/NAME
;; and a symlink is made from $GIT_DIR/DIR/NAME to $GIT_DIR/zow-feed.

;;; Code:

(require 'cl-lib)
(require 'pcase)
(require 'bytecomp)

(defvar zow-so-it-goes
  '("fix problem and try again"
    "save time and give up now"
    "probably not worth it anyway"
    "consider switching career to VCR repair"
    "no worries -- unlikely the boss can do better"
    ;; Add more stoic sagacity here.
    "relax and try ‘M-x zone’ --ttn"
    "a pity about ‘M-x yow’ --ttn"
    "oop ack! --bill the cat")
  "List of (perhaps, perhaps not) consoling blurbs.")

(defvar zow-mode-map
  ;; adapted from ‘messages-buffer-mode-map’
  (let ((map (make-sparse-keymap)))
    (set-keymap-parent map special-mode-map)
    (define-key map "\C-c\C-s" 'zow-stop)
    (define-key map "\C-c\C-q" 'zow-continue)
    (define-key map "\C-c\C-t" 'zow-truncate-buffer)
    (define-key map "\C-c\C-k" 'zow-terminate)
    (define-key map "g" nil)            ; nothing to revert
    map))

(defvar zow--cleanup nil
  "List of cleanup specifications for command ‘zow--cleanup’.
Each element is either a FILENAME, absolute, to be deleted,
or the form (FILE NEWNAME), to be passed to ‘rename-file’.")

(defun zow-version ()
  (interactive)
  (let ((v (eval-when-compile
             (format "ZOW %s of %s on %s"
                     (or (ignore-errors
                           (substring
                            (shell-command-to-string
                             (if (file-directory-p ".git")
                                 "git describe --long"
                               "autoconf --trace 'AC_INIT:$2'"))
                            0 -1))
                         (getenv "VERSION")
                         "UNKNOWN")
                     (format-time-string "%F")
                     (system-name)))))
    (if (called-interactively-p 'interactive)
        (message "%s" v)
      v)))

(defun zow--function-signature (fn)
  (let (sym-fn)
    ;; Loop snarfed from GNU Emacs 24.4 ‘byte-compile-fdefinition’
    ;; and modified to use ‘sym-fn’.  Thanks, bytecomp.el!
    (while (and (symbolp fn)
                (fboundp fn)
                (or (symbolp (setq sym-fn (symbol-function fn)))
                    (consp sym-fn)
                    (byte-code-function-p sym-fn)))
      (setq fn sym-fn)))
  ;; Below[*], we proceed deliberately despite (info "(elisp) Closures")
  ;;  | However, the fact that the internal structure of a closure is
  ;;  | “exposed” to the rest of the Lisp world is considered an internal
  ;;  | implementation detail.  For this reason, we recommend against
  ;;  | directly examining or altering the structure of closure objects.
  ;; (Emacs) FIXME: Push for some API; use it.
  (byte-compile-arglist-signature
   (pcase fn
     ((pred byte-code-function-p) (aref fn 0))
     (`(lambda ,arglist . ,_)      arglist)
     (`(closure ,_ ,arglist . ,_)  arglist) ;;; [*]
     (_                            nil))))

(defalias 'zow-ctxt-conn 'car
  "Return the connection component of CONTEXT.
You can use ‘process-contact’ on this object, for example.

\(fn CONTEXT\)")

(defalias 'zow-ctxt-name 'cdr
  "Return the name (a symbol) component of CONTEXT.

\(fn CONTEXT\)")

(define-error 'zow-server-error "SERVER-ERROR")

(define-error 'zow-transaction-error "Transaction error")

(defun zow-transaction-error (fmt &rest args)
  (signal 'zow-transaction-error (cons fmt args)))

(defmacro zow-with-GIT_DIR (dir &rest body)
  (declare (indent 1))
  `(let ((process-environment (cons (concat "GIT_DIR=" ,dir)
                                    process-environment)))
     ,@body))

(defun zow--enquire (conn name body args)
  (let* ((info (or (gethash name (process-get conn :dispatch))
                   (signal 'zow-server-error
                           (list "No handler for ‘%s’"
                                 name))))
         (func (cdr info))
         (ctxt (cons conn name))
         (ans (with-current-buffer (process-get conn :buf)
                (let ((default-directory (process-get conn :default-directory)))
                  ;; Check ‘args’ here, immediately prior to ‘apply’, in
                  ;; order to mimic as closely as possible the environment
                  ;; of the "native" ‘wrong-number-of-arguments’ error that
                  ;; would be thrown by the ‘apply’.
                  (cl-destructuring-bind (min . max)
                      (car info)
                    (when max
                      (let ((count (length args)))
                        (unless (<= min (+ 2 count) max)
                          (zow-transaction-error
                           ;; adapted from ‘byte-compile-callargs-warn’
                           "Handler%s for ‘%s’ called with %d arg%s, but %s %s"
                           (if (symbolp func)
                               (format " ‘%s’" func)
                             "")
                           name
                           count
                           (if (= 1 count) "" "s")
                           (if (< (+ 2 count) min)
                               "requires"
                             "accepts only")
                           (byte-compile-arglist-signature-string
                            (cons (- min 2)
                                  (- max 2))))))))
                  (apply func ctxt body args)))))
    (unless (and ans (consp ans))
      (setq ans (list ans)))
    (if (consp (cdr ans))
        ;; Avoid ‘(setcdr ans ...)’ and ‘(setf (cdr ans) ...)’
        ;; to sidestep "static" data corruption bugs.
        (cons (car ans)
              (mapconcat #'identity
                         (cdr ans)
                         "\n"))
      ans)))

(defun zow--note (proc who fmt &rest args)
  (let ((buf (process-buffer proc)))
    (when (buffer-live-p buf)
      (with-current-buffer buf
        (let* ((mark (or (process-mark proc)
                         (error "No process mark for %S" proc)))
               (moving (= mark (point)))
               (inhibit-read-only t))
          (save-excursion
            (goto-char mark)
            (if who
                (insert who (format-time-string " [%F %T.%6N] ")
                        (apply #'format fmt args))
              (insert fmt))             ; ignore args
            (unless (bolp)
              (newline))
            (set-marker mark (point)))
          (when moving
            (goto-char mark)))))))

(defun zow--connection-sentinel (conn string)
  (let* ((now (process-status conn))
         (bye (eq 'closed now))
         (bacc (process-get conn :bacc))
         (size (and bye (buffer-size bacc))))
    (zow--note (process-get conn :server) ""
               "(%s%s) %s"
               now (if (and size (< 0 size))
                       (format ", buffer size: %d" size)
                     "")
               string)
    (when bye
      (kill-buffer bacc))))

(defun zow--get-buffer-create (name)
  (let ((buf (get-buffer-create name)))
    (buffer-disable-undo buf)
    buf))

(defun zow--log (server conn string)
  (set-process-sentinel conn #'zow--connection-sentinel)
  (set-process-plist
   conn (cl-list*
         :bacc (zow--get-buffer-create (concat " " (process-name conn)))
         :stage (list 0)
         :server server
         (process-plist conn)))
  (zow--note server "\n" "(%s %s) %s"
             (process-status server)
             (process-status conn)
             string))

(defun zow--server-sentinel (server string)
  (let ((now (process-status server)))
    (zow--note server "" "(%s) %s"
               now string)
    (cl-case now
      ((open closed))
      (t (user-error "Weirdness w/ %s" (process-name server))))))

(defalias 'zow--stage-header 'cadr)
(defalias 'zow--stage-blines 'cddr)

(define-error 'zow-syntax-error "Syntax error")

(defun zow--signal-syntax-error (fmt &rest args)
  (signal 'zow-syntax-error (cons fmt args)))

(defun zow-maybe-s (n blurb)
  (format "%d %s%s"
          n blurb
          (if (= 1 n)
              ""
            "s")))

(defun zow--gather (stage string)
  (insert string)
  (cl-loop
   with buf = (current-buffer)
   with from
   with eol
   do (cl-flet
          ((garbage (fmt &rest args)
                    (apply #'zow--signal-syntax-error
                           (concat "Garbage " fmt)
                           args))
           (to-eol (from)
                   (buffer-substring from eol)))
        (cl-case (car stage)
          (0 (if (> 4 (buffer-size))
                 (cl-return)
               (setq from (point-min))
               (cl-flet
                   ((snarf (n) (delete-and-extract-region from (+ n from))))
                 (pcase (snarf 3)
                   ("" t)
                   (magic (zow--signal-syntax-error
                           "Bad magic: %s"
                           (mapconcat (lambda (c)
                                        (format "%02x" c))
                                      magic
                                      " "))))
                 (pcase (string-to-char (snarf 1))
                   (1 t)
                   (version (zow-transaction-error
                             "Unsupported protocol version: %d"
                             version))))
               (cl-incf (car stage))))
          (1 (if (not (search-backward "\n" nil t))
                 (cl-return)
               (goto-char (point-min))
               (setq eol (line-end-position))
               (cl-flet
                   ((grok (role okp)
                          (cl-flet
                              ((bad (badness) (zow--signal-syntax-error
                                               "%s %s" badness role)))
                            (if (looking-at "\\s-*$")
                                (bad "Missing")
                              (setq from (point))
                              (let ((v (condition-case ugh
                                           (read buf)
                                         (error
                                          (bad
                                           (format
                                            "[%s %S] Malformed"
                                            (let ((e (car ugh)))
                                              (or (get e 'error-message)
                                                  e))
                                            (to-eol from)))))))
                                (if (funcall okp v)
                                    v
                                  (bad "Invalid")))))))
                 (let* ((numlines (grok "line count"
                                        #'integerp))
                        (header (grok "header"
                                      (lambda (x)
                                        (and x
                                             (listp x)
                                             (symbolp (car x)))))))
                   (unless (looking-at "\\s-*$")
                     (garbage "at end of header line: %S"
                              (to-eol (point))))
                   (delete-region (point-min) (1+ eol))
                   (goto-char (point-max))
                   (cl-incf (car stage))
                   (setcdr stage (list numlines header))))))
          (2 (let ((exp (cadr stage))
                   (got (count-lines
                         (point-min)
                         (point))))
               (if (> exp got)
                   (cl-return)
                 (cl-incf (car stage))
                 (let ((data (cdr stage)))
                   (setcar data (cadr data))
                   (setcdr data (unless (zerop exp)
                                  (goto-char (point-min))
                                  (end-of-line exp)
                                  (prog1 (split-string
                                          (delete-and-extract-region
                                           (point-min) (point))
                                          "\n")
                                    (unless (eql ?\n (char-after))
                                      (zow--signal-syntax-error
                                       "Missing newline after %s"
                                       (zow-maybe-s exp "expected line")))
                                    (delete-char 1))))))))
          (3 (if (zerop (buffer-size))
                 (cl-return)
               (garbage "following %s: %s, %s"
                        (zow-maybe-s (length (zow--stage-blines stage))
                                     "expected line")
                        (zow-maybe-s (count-lines (point-min) (point-max))
                                     "line")
                        (zow-maybe-s (buffer-size buf)
                                     "char"))))
          ((4 t) (error "Bad stage: %S" stage))))))

(defun zow--filter (conn string)
  (let* ((server (process-get conn :server))
         (stage (process-get conn :stage))
         (bacc (process-get conn :bacc))
         (problem nil)
         (total (with-current-buffer bacc
                  (condition-case ugh
                      (zow--gather stage string)
                    (error
                     (setq problem ugh)))
                  (buffer-size)))
         (header (when (< 2 (car stage))
                   (zow--stage-header stage)))
         (header-name (car header)))
    (cl-flet
        ((nb (fmt &rest args) (apply #'zow--note server "" fmt args)))
      (nb "+ %5d => %5d  %S"
          (length string)
          total
          (if header
              (list (car stage) '-- header-name)
            stage))
      (when (or problem (= 3 (car stage)))
        (cl-destructuring-bind
            (res . s) (cl-labels
                          ((fm (cdata &optional wrap)
                               (if wrap
                                   (fm (list wrap "%s" (fm cdata)))
                                 (cl-destructuring-bind (bad fmt &rest args)
                                     cdata
                                   (let ((prefix (or (get bad 'error-message)
                                                     "")))
                                     (concat (if (string= "" prefix)
                                                 prefix
                                               (concat prefix ": "))
                                             (apply #'format
                                                    (or fmt (mapconcat
                                                             #'identity
                                                             (make-list
                                                              (length args)
                                                              "%S")
                                                             ", "))
                                                    args))))))
                           (sigh (x &optional wrap)
                                 (cons nil (fm x wrap))))
                        (if problem
                            (sigh problem)
                          (condition-case ugh
                              (zow--enquire conn header-name
                                            (zow--stage-blines stage)
                                            (cdr header))
                            ((zow-transaction-error
                              zow-server-error)
                             (sigh ugh))
                            (error
                             (push (pcase ugh
                                     (`(,(pred symbolp)
                                        ,(pred stringp))
                                      ;; orig: (SYMBOL STRING)
                                      ;; want: (SYMBOL "%s" STRING)
                                      "%s")
                                     (_
                                      ;; orig: (SYMBOL [...])
                                      ;; want: (SYMBOL nil [...])
                                      nil))
                                   (cdr ugh))
                             (sigh ugh (cl-case (car ugh)
                                         (user-error 'zow-transaction-error)
                                         (t 'zow-server-error)))))))
          (cl-flet
              ((inscrutable ()
                            (zerop (length s)))
               (say (x)
                    (nb " %s" x)
                    (process-send-string conn x))
               (one (x)
                    (format "%s\n" x))
               (two (x y)
                    (format "%s %s\n" x y)))
            (if res
                (unless (inscrutable)
                  (say (one s)))
              (setq res 'FAIL)
              (say (if (inscrutable)
                       (one res)
                     (two res s)))))
          (process-send-eof conn))
        ;; Don't use ‘interrupt-process’ as it results in (e.g.):
        ;; error in process filter: progn: Process PROC-NAME is not a subprocess
        (delete-process conn)))))

(defun zow-get-process-dammit ()
  (if (eq 'zow-mode major-mode)
      (get-buffer-process (current-buffer))
    (user-error "Not a ZOW buffer")))

(defsubst zow--ends-w-/ (string)
  (= ?/ (aref string (1- (length string)))))

(defun zow--cleanup ()
  (when (eq 'zow-mode major-mode)
    (let* ((proc (zow-get-process-dammit))
           (sockname (ignore-errors (process-get proc :sockname))))
      (cl-flet
          ((zonk (filename)
                 (condition-case nil
                     (progn
                       (funcall (if (zow--ends-w-/ filename)
                                    #'delete-directory
                                  #'delete-file)
                                filename)
                       t)
                   (file-error
                    nil))))
        (cl-loop
         for elt in zow--cleanup
         if (stringp elt)
         do (zonk elt)
         else do (ignore-errors (apply #'rename-file elt)))
        (setq zow--cleanup nil)
        (when sockname
          (let ((fmt (if (zonk sockname)
                         "Socket %S deleted"
                       "No socket %S")))
            (message fmt sockname)
            (when proc
              (zow--note proc "" fmt
                         (file-relative-name sockname)))))))))

(defun zow-kill-emacs-hook ()
  (dolist (buf (buffer-list))
    (with-current-buffer buf
      (zow--cleanup))))

(defun zow-call-git (fmt &rest args)
  (let ((s (shell-command-to-string
            (apply #'format
                   (concat "git " fmt)
                   args))))
    (if (zerop (length s))
        s
      (substring s 0 -1))))

(defun zow-truly-git-config (fmt &rest args)
  (string= "true" (apply #'zow-call-git (concat "config " fmt) args)))

(defun zow-call-git-for-status (fmt &rest args)
  (string-to-number
   (shell-command-to-string
    (apply #'format
           (concat "git " fmt " 1>/dev/null 2>&1 ; echo $?")
           args))))

(define-derived-mode zow-mode special-mode "ZOW"
  "Major mode used in the ZOW server buffers."
  (set (make-local-variable 'zow--cleanup) nil)
  (setq mode-line-process '(":%s")
        truncate-lines t))

(defun zow-add-handlers (&rest hsets)
  (let ((proc (zow-get-process-dammit))
        (target (expand-file-name "zow-feed"))
        (dirs (mapcar #'car hsets))
        ok)
    (pcase (process-status proc)
      (`stop)
      (status (user-error "ZOW status: %s" status)))
    ;; First pass: Validate dir and handlers.
    (dolist (hset hsets)
      (cl-loop
       with mm
       with dir = (car hset)            ; HSET is (DIR . ALIST)
       for (name . func) in (cdr hset)
       do (when dir
            (cl-flet
                ((bad (badness)
                      (user-error
                       (concat "Dir name " badness ": %S")
                       dir)))
              (cond ((not (stringp dir))
                     (bad "not a string"))
                    ((zerop (length dir))
                     (bad "is empty"))
                    ((file-name-absolute-p dir)
                     (bad "not relative"))
                    ((not (zow--ends-w-/ dir))
                     (bad "does not end with ‘/’ (slash)"))
                    ((string-match "[.][.]/" dir)
                     (bad "has escaping component"))
                    ((let ((sans-/ (directory-file-name dir)))
                       (and (file-exists-p sans-/)
                            (not (file-directory-p sans-/))))
                     (bad "already exists as a non-directory file")))))
       do (setq mm (zow--function-signature func))
       do (let ((max (cdr mm)))
            (when (and max (> 2 max))
              (user-error "Handler for ‘%s’ does not accept 2+ args%s"
                          name
                          (if (symbolp func)
                              (format ": %s" func)
                            ""))))
       do (push (list name dir
                      ;; info
                      (cons mm func))
                ok)))
    (cl-flet
        ((btw (x)
              (push x zow--cleanup))
         (nb (fmt &rest args)
             (apply #'zow--note proc "" fmt args)))
      (dolist (dir dirs)
        (when (and dir (not (file-directory-p dir)))
          (setq dir (file-name-as-directory dir))
          (make-directory dir t)
          (btw (expand-file-name dir))
          (nb "Created %S" dir)))
      (setq dirs (mapcar #'list dirs))
      (cl-loop
       with ht = (process-get proc :dispatch)
       for (name dir info) in ok
       do (puthash (if dir
                       (intern (format "/%s%s"
                                       (if (string= "./" dir)
                                           ""
                                         dir)
                                       name))
                     name)
                   info ht)
       do (push name (cdr (assoc-string dir dirs)))
       when dir
       do (let* ((base (symbol-name name))
                 (default-directory (expand-file-name dir))
                 (filename (expand-file-name base)))
            (when (file-exists-p filename)
              (let ((saved (concat filename ".SAVE")))
                (rename-file filename saved t)
                (btw (list saved filename))))
            (make-symbolic-link (file-relative-name target)
                                base)
            (btw filename)))
      (cl-loop
       for (dir . names) in dirs
       when names
       do (nb "Added %s (%s): %s"
              (zow-maybe-s (length names) "handler")
              (if dir
                  (format "in %S" (file-name-as-directory dir))
                "internal")
              (mapconcat #'symbol-name
                         names
                         ", "))))))

(defun zow-terminate (&optional quietly)
  (interactive "P")
  (let ((proc (zow-get-process-dammit)))
    (zow--cleanup)
    (if quietly
        (set-process-sentinel proc #'ignore)
      (zow--note proc "\n" "Terminating!")
    (delete-process proc))))

(defun zow--flow-control (proc want frob blurb)
  (if (eq want (process-status proc))
      (message "(Process ‘%s’ already in ‘%s’ state)" proc want)
    (funcall frob proc)
    (zow--note proc "" "%s on socket %S"
               blurb
               (file-relative-name
                (process-get proc :sockname)))
    (force-mode-line-update)))

(defun zow-stop (proc)
  (interactive (list (zow-get-process-dammit)))
  (zow--flow-control proc 'stop #'stop-process "Stopped listening"))

(defun zow-continue (proc)
  (interactive (list (zow-get-process-dammit)))
  (zow--flow-control proc 'listen #'continue-process "Listening"))

;; Hack to represent file zow-feed as a docstring so that byte-compiling
;; moves it on disk (in zow.elc) w/o requiring residency for access/use.
;; This way, we don't need to install that file.  Downside: subsequent
;; out-of-tree byte-compilation or source (.el) evaluation will lose.
(eval-when-compile
  (let* ((filename "zow-feed")
         (form `(defun zow--create-zow-feed (dir)
                  ,(with-temp-buffer
                     (insert-file-contents filename)
                     (buffer-string))
                  (let ((copy (expand-file-name ,filename dir)))
                    (with-temp-file copy
                      (insert "#!/bin/bash\n"
                              (format "export GIT_DIR='%s'\n" dir)
                              (documentation 'zow--create-zow-feed t))
                      ;; Zonk "\n\\(fn ...)", maybe.  This is not strictly
                      ;; necessary, as the script calls ‘exit’ explicitly.
                      ;; However, "cleanliness is next to fordliness"...
                      (when (search-backward "ends here\n"
                                             (line-beginning-position -2)
                                             t)
                        (delete-region (match-end 0)
                                       (point-max))))
                    copy))))
    (if byte-compile-current-buffer
        (save-excursion
          (let ((print-escape-newlines t))
            ;; Prefer ‘prin1’ to ‘print’ to preserve accuracy of line
            ;; numbers in downstream diagnostic messages (if any :-D).
            (prin1 form (current-buffer))))
      (eval form))))

(defun zow-dir (dir &rest hsets)
  (dolist (filename '("HEAD" "branches/" "config" "info/" "objects/" "refs/"))
    (unless (funcall (if (string-match "/$" filename)
                         #'file-directory-p
                       #'file-exists-p)
                     (expand-file-name filename dir))
      (user-error "Improper (missing %S) repo dir: %s"
                  filename dir)))
  (let* ((default-directory (file-name-as-directory
                             (expand-file-name dir)))
         (sans-/ (directory-file-name default-directory))
         (bare (zow-truly-git-config "core.bare"))
         (sockname (expand-file-name "zow.sock"))
         (basename (if bare
                       (file-name-nondirectory sans-/)
                     (substring sans-/ (length (expand-file-name
                                                "../../")))))
         (buf (switch-to-buffer
               (zow--get-buffer-create
                (concat " " basename))))
         (proc (get-buffer-process buf)))
    (when proc
      (list-processes)
      (user-error "Process already exists: %s" proc))
    (zow-mode)
    (when (file-exists-p sockname)
      (delete-file sockname))
    (setq proc (make-network-process
                :name      "zow"
                :server     t
                :noquery    t
                :stop       t
                :family    'local
                :service    sockname
                :buffer     buf
                :log      #'zow--log
                :filter   #'zow--filter
                :sentinel #'zow--server-sentinel))
    (process-put proc :default-directory default-directory)
    (process-put proc :dispatch (make-hash-table :size 11
                                                 :test 'eq))
    (process-put proc :sockname sockname)
    (process-put proc :bare bare)
    (process-put proc :buf buf)
    (add-hook 'kill-buffer-hook
              #'zow--cleanup
              nil t)
    (set (make-local-variable 'process-environment)
         (copy-sequence process-environment))
    (setenv "GIT_DIR" sans-/)
    (cl-flet
        ((hey (fmt &rest args)
              (apply #'zow--note proc "" fmt args)))
      (hey "-*- default-directory: %S -*-"
           (abbreviate-file-name default-directory))
      (hey "Created socket %S" (file-relative-name sockname))
      (let ((copy (zow--create-zow-feed sans-/)))
        (set-file-modes copy #o555)
        (push copy zow--cleanup)
        (hey "Created %S" (file-relative-name copy)))
      (pcase (condition-case ugh
                 (when hsets
                   (apply #'zow-add-handlers hsets)
                   nil)
               (user-error
                (hey "Could not add handlers %s -- %s"
                     (cdr ugh)
                     (nth (random (length zow-so-it-goes))
                          zow-so-it-goes))
                ugh)
               (error
                ugh))
        (`(,symbol . ,data)
         (zow-terminate t)
         (signal symbol data))))
    (zow-continue proc)
    proc))

(defun zow-truncate-buffer ()
  (interactive)
  (let* ((p (line-beginning-position))
         (max (point-max))
         (bef (count-lines (point-min) p))
         (aft (count-lines p max))
         (inhibit-read-only t))
    (delete-region p max)
    (cl-flet
        ((lines (n) (zow-maybe-s n "line")))
      (message "%s truncated, %s remaining"
               (lines aft)
               (lines bef)))))

(defun zow-assert-no-body (ctxt body)
  (when body
    (zow-transaction-error
     "Unexpected body (%s) in ‘%s’"
     (zow-maybe-s (length body)
                  "line")
     (zow-ctxt-name ctxt)))
  t)

(defun zow-msg (fmt &rest args)
  (zow--note
   (zow-get-process-dammit)             ; FIXME: svelten
   nil (apply #'format fmt args)))

(defmacro zow--normalize-args (v)
  `(when (and (= 1 (length ,v))
              (or (not (car ,v))
                  (consp (car ,v))))
     (setq ,v (car ,v))))

(defun zow-yeah-whatever (ctxt body &rest args)
  (zow--normalize-args args)
  (zow-msg "%d %S"
           (length body)
           (cons (zow-ctxt-name ctxt)
                 args))
  (dolist (line body)
    (zow-msg "%s" line))
  t)

(defun zow-yeah-echo (ctxt body &rest args)
  (zow--normalize-args args)
  (cl-list* t (format "%d %S"
                      (length body)
                      (cons (zow-ctxt-name ctxt)
                            args))
            body))

;;;---------------------------------------------------------------------------
;;; that's it!

(add-to-list 'kill-emacs-hook 'zow-kill-emacs-hook)

(provide 'zow)

;;; zow.el ends here
