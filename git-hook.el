;;; git-hook.el                                     -*- lexical-binding: t -*-

;; Copyright (C) 2016-2021 Thien-Thi Nguyen
;;
;; This file is part of ZOW.
;;
;; ZOW is free software; you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free
;; Software Foundation; either version 3, or (at your option) any later
;; version.
;;
;; ZOW is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
;; for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with ZOW.  If not, see <https://www.gnu.org/licenses/>.

(eval-when-compile (require 'rx))
(require 'cl-lib)
(require 'pcase)
(require 'zow)

(defconst git-hook-handlers
  '((update . git-hook-update)
    (pre-receive . git-hook-pre/post-receive)
    (post-receive . git-hook-pre/post-receive)))

(defconst git-hook-local-handlers
  '((pre-push . git-hook-pre-push)))

(defun git-hook-all0p (s)
  (string= "0000000000000000000000000000000000000000"
           s))

(defun git-hook-characterize (refname newrev)
  (let (type ref)
    (when (string-match "^refs/\\([^/]+\\)/\\(.+\\)" refname)
      (setq type (intern (match-string 1 refname))
            ref (match-string 2 refname)))
    (cl-list*
     ref
     ;; if ‘newrev’ is 0000...0000, it's a commit to delete a ref.
     (if (git-hook-all0p newrev)
         'delete
       (intern (zow-call-git "cat-file -t %s"
                             newrev)))
     type)))

;;;---------------------------------------------------------------------------
;;; "remote" (server side)

(cl-defun git-hook-update (ctxt body refname oldrev newrev)
  "Emulate /usr/share/git-core/templates/hooks/update.sample for ZOW.
See ‘zow-handlers’ for CTXT and BODY, and (man \"githooks(5)\")
for REFNAME, OLDREV, NEWREV.

The configuration vars are hooks.NAME (man \"git-config(1)\")
w/ default value ‘false’.
 allowunannotated  -- Allow adding unannotated tags?
 allowdeletetag    -- Allow deleting tags?
 allowmodifytag    -- Allow modifying a tag after creation?
 allowdeletebranch -- Allow deleting branches?
 denycreatebranch  -- Deny remotely creating branches?

For example, to allow deleting branches, use:
$ git config --bool hooks.allowdeletebranch true"
  (zow-assert-no-body ctxt body)
  (cl-flet
      ((var-p (v)
              (zow-truly-git-config
               "--bool hooks.%s"
               v))

       (w/* (fmt &rest args)
            (apply #'format
                   (concat "*** " fmt)
                   args))

       (w/$0 (s)
             (format s (zow-ctxt-name ctxt)))

       (die (&rest lines)
            (cl-return-from git-hook-update
              (cons nil lines))))

    (let ((verbotten "is not allowed in this repository")
          (git-dir (or (getenv "GIT_DIR")
                       (die
                        "Don't run this script from the command line."
                        " (if you want, you could supply GIT_DIR then run"
                        (w/$0 "  %s <ref> <oldrev> <newrev>)")))))

      (when (cl-some (lambda (x)
                       (or (not (stringp x))
                           (zerop (length x))))
                     (list refname oldrev newrev))
        (die
         (w/$0 "usage: %s <ref> <oldrev> <newrev>")))

      ;; check for no description
      (unless (ignore-errors
                (not (with-temp-buffer
                       (insert-file-contents
                        (expand-file-name "description" git-dir))
                       (looking-at "^Unnamed repository"))))
        (die
         (w/* "Project description file hasn't been set")))

      (pcase (git-hook-characterize refname newrev)

        ;; un-annotated tag
        (`(,tag commit . tags)
         (unless (var-p 'allowunannotated)
           (die
            (w/* "The un-annotated tag ‘%s’ %s"
                 tag
                 verbotten)
            (w/* "Use 'git tag [ -a | -s ]' for tags you want to propagate."))))

        ;; delete tag
        (`(,_ delete . tags)
         (unless (var-p 'allowdeletetag)
           (die
            (w/* "Deleting a tag %s" verbotten))))

        ;; annotated tag
        (`(,tag tag . tags)
         (when (and (not (var-p 'allowmodifytag))
                    (zerop (zow-call-git-for-status
                            "rev-parse %s" refname)))
           (die
            (w/* "Tag ‘%s’ already exists." tag)
            (w/* "Modifying a tag %s." verbotten))))

        ;; branch
        (`(,_ commit . heads)
         (when (and (git-hook-all0p oldrev)
                    (var-p 'denycreatebranch))
           (die
            (w/* "Creating a branch %s" verbotten))))

        ;; delete branch
        (`(,_ delete . heads)
         (unless (var-p 'allowdeletebranch)
           (die
            (w/* "Deleting a branch %s" verbotten))))

        ;; tracking branch
        (`(,_ commit . remotes)
         t)

        ;; delete tracking branch
        (`(,_ delete . remotes)
         (unless (var-p 'allowdeletebranch)
           (die
            (w/* "Deleting a tracking branch %s" verbotten))))

        ;; Anything else (is there anything else?)
        (`(,_ ,type . ,_)
         (die
          (w/* "Update hook: unknown type of update to ref ‘%s’ of type ‘%s’"
               refname type))))

      ;; --- Finished
      t)))

(defun git-hook-pre/post-receive (ctxt body)
  (cl-loop
   with pre = (eq '/hooks/pre-receive (zow-ctxt-name ctxt))
   for (oldrev newrev refname) in (mapcar #'split-string body)
   do (pcase (git-hook-characterize refname newrev)
        (`(,branch commit . heads)
         (if (git-hook-all0p oldrev)
             (zow-msg "new branch: %s" branch)
           (zow-msg "on branch ‘%s’" branch)
           (unless pre
             (cl-loop
              for line in (split-string
                           (zow-call-git "diff --numstat -z %s %s"
                                         oldrev newrev)
                           "\0" t)
              do (if (not (string-match
                           (rx bos (group (or ?- (1+ (in digit))))
                               ?\t (group (or ?- (1+ (in digit))))
                               ?\t (group (1+ nonl))
                               eos)
                           line))
                     (error "Cannot parse %S %S"
                            (type-of line)
                            line)
                   (let ((pl (match-string 1 line))
                         (mi (match-string 2 line))
                         (fn (match-string 3 line)))
                     (if (and (string= "-" pl)
                              (string= "-" mi))
                         (zow-msg "binary %S" fn)
                       (zow-msg "+%s -%s %S" pl mi fn))))))))
        (`(,ref ,op . ,type)
         (zow-msg "(fyi) op ‘%s’ on ‘%s’ in %s"
                  op ref type))))
  t)

;;;---------------------------------------------------------------------------
;;; local

(defun git-hook-pre-push (_ctxt body remote url)
  (zow-msg "%s to remote ‘%s’ (%s)"
           (zow-maybe-s (length body) "change")
           remote url)
  (cl-loop
   with count = 0
   for (l-ref l-sha r-ref r-sha) in (mapcar #'split-string body)
   do (zow-msg "%d.\tL: %s %s\n\tR: %s %s"
               (cl-incf count) l-sha l-ref r-sha r-ref)
   do (if (git-hook-all0p l-sha)
          ;; Handle delete
          t
        (let ((range (if (git-hook-all0p r-sha)
                         ;; New branch, examine all commits
                         l-sha
                       ;; Update to existing branch, examine new commits
                       (format "%s..%s" r-sha l-sha))))
          ;; Check for WIP commit
          (pcase (zow-call-git "rev-list -n 1 --grep '^WIP' %s"
                               range)
            ("")
            (commit (cl-return
                     (cons nil
                           (format "Found WIP commit in ‘%s’ (%s), not pushing"
                                   l-ref commit)))))))
   finally
   return
   t))

;;;---------------------------------------------------------------------------
;;; kick

(defun git-hook-kick (dir &optional strategy)
  (interactive
   (list (read-directory-name "Repo directory: ")
         (when current-prefix-arg
           (intern (completing-read
                    "Handler selection strategy: "
                    '("auto" "local" "remote" "both")
                    nil                 ; PREDICATE
                    t                   ; REQUIRE-MATCH
                    nil                 ; INITIAL-INPUT
                    nil                 ; HIST
                    "auto"              ; DEF
                    nil)))))
  (zow-dir dir (cons "hooks/"
                     (cl-ecase (or strategy 'auto)
                       (auto   (if (zow-with-GIT_DIR (expand-file-name dir)
                                     (zow-truly-git-config "core.bare"))
                                   git-hook-handlers
                                 git-hook-local-handlers))
                       (local  git-hook-local-handlers)
                       (remote git-hook-handlers)
                       (both   (append git-hook-local-handlers
                                       git-hook-handlers))))))

;;;---------------------------------------------------------------------------
;;; that's it

(provide 'git-hook)

;;; git-hook.el ends here
