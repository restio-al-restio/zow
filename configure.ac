# configure.ac
#
# Copyright (C) 2016-2021 Thien-Thi Nguyen
#
# This file is part of ZOW.
#
# ZOW is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 3, or (at your option) any later
# version.
#
# ZOW is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU General Public License
# along with ZOW.  If not, see <https://www.gnu.org/licenses/>.

AC_INIT([ZOW],[0.4],[ttn@gnuvola.org])

AC_ARG_VAR([NETCAT], [name of netcat(1) program that supports ‘-U’])
AC_CHECK_PROG([NETCAT], [netcat], [netcat])
AS_IF([test "x$NETCAT" = x],
  [AC_MSG_ERROR([netcat not found; required!])])
AC_CACHE_CHECK([if $NETCAT can handle -U],[zow_cv_netcat_minus_U],[
AS_IF([$NETCAT -U 2>&1 | grep 'invalid option' >/dev/null 2>&1],
  [zow_cv_netcat_minus_U=no],
  [zow_cv_netcat_minus_U=yes])
])
AS_IF([! test yes = $zow_cv_netcat_minus_U],
  [AC_MSG_ERROR([Sorry, $NETCAT does not support ‘-U’])])

# allow env override but do not get fooled by EMACS=t
test t = "$EMACS" && unset EMACS
AC_ARG_VAR([EMACS], [name of emacs(1) program for building ZOW])
# the next line does nothing if var EMACS is already set
AC_CHECK_PROG([EMACS], [emacs], [emacs])
AS_IF([test "x$EMACS" = x],
  [AC_MSG_ERROR([emacs not found; required!])])

AC_CACHE_CHECK([if $EMACS can handle -L DIR],[zow_cv_emacs_minus_L],[
AS_IF([$EMACS --batch --no-site-file -L . --kill >/dev/null 2>&1],
  [zow_cv_emacs_minus_L=yes],
  [zow_cv_emacs_minus_L=no])
])
AS_IF([! test yes = $zow_cv_emacs_minus_L],
  [AC_MSG_ERROR([Sorry, EMACS=$EMACS cannot build ZOW])])

AC_ARG_WITH([sitelisp],
 [AS_HELP_STRING([--with-sitelisp=DIR],
   [Override the default site-lisp directory
    (default: '${datarootdir}/emacs/site-lisp')])],
 [sitelisp="$withval"],
 [sitelisp="$datadir/emacs/site-lisp"])
AC_SUBST([sitelisp])

AC_CONFIG_FILES([zow-feed],
       [chmod +x zow-feed])

AC_CONFIG_FILES([
  doc/GNUmakefile
  t/GNUmakefile
  GNUmakefile
])
AC_OUTPUT

# configure.ac ends here
